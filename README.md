[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=nelson.frugeri_metrics&metric=alert_status)](https://sonarcloud.io/dashboard?id=nelson.frugeri_metrics)

# Metrics

> Metrics is the library responsible for collect metrics and send to monitoring and
observability providers

![image](https://s-media-cache-ak0.pinimg.com/736x/91/7b/91/917b91356dd1f9c4a89585ead802e261.jpg)

**The following are the events and their respective fields and use cases**

### METRICS_REQUEST
| **Field** | **Case** |
| --- | --- |
| request-id | success/failure |
| host | success/failure |
| request-headers | success/failure |
| request-method | success/failure |
| request-uri | success/failure |
| request-query-params | success/failure |
| method-name | success/failure |
| latency-time | success/failure |
| status-code | success/failure |
| response-headers | success |
| response-body | success |

### METRICS_EXCEPTION
| **Field** |
| --- |
| request-id |
| method-name |
| exception-message |
| exception-cause |

## How to use in your API

**Configure in application.yml the adapters**
```
metrics:
  adapters: newrelic,logstash
```

Supported Adapters
* [NewRelic](https://newrelic.com/)
* [Logstash](https://www.elastic.co/pt/logstash)

**Declare the Spring Bean**
```
@Bean
public Metrics netrics() {
  return new Metrics();
}
```

**Use the annotation @Metrics in controller methods**
```
@Metrics
@GetMapping(value = "/search")
public ResponseEntity<PayloadDtoTest> findSearch(
    @ModelAttribute("search") final String search) {
  
  return ResponseEntity.ok(PayloadDtoTest.builder()
    .id(1).build());
}
```

**In order for the METRICS_REQUEST event to be sent in case of failure, it is necessary to have the @ExceptionHandler and @ResponseStatus annotations in your controller advice**
```
@ExceptionHandler(Exception.class)
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
@ResponseBody
public ResponseEntity<?> exception(final Exception ex) {
  return ResponseEntity.unprocessableEntity().build();
}
```

## Requirements
```sh
Java 11
```

## Install in Linux:

**Java 11 - SDKMAN:**

* [SdkMan](https://sdkman.io/install)

**Lombok plugin:**

* [Intellij](https://projectlombok.org/setup/intellij)
* [Eclipse](https://projectlombok.org/setup/eclipse)

## How to test the API

**Run the tests:**

```sh
$ mvn test
```

## How to install the library

**Run to install:**

```sh
$ mvn clean install
```
