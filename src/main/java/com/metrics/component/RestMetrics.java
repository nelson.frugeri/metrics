package com.metrics.component;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RestMetrics {
  String messageComplete() default "METRICS_COMPLETE";
  String messageException() default "METRICS_EXCEPTION";
}
