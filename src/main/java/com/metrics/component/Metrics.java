package com.metrics.component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.metrics.service.MetricType;
import com.metrics.service.MetricsService;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Aspect
public class Metrics {

  @Value("${metrics.services.logstash.enabled}")
  private boolean isLogstash;

  private ConcurrentMap<String, Object> data;

  private final ObjectMapper objectMapper = new ObjectMapper();

  @Pointcut(value = "@annotation(RestMetrics)")
  public void restMetrics() {
    // Designed to capture the initializer execution of annotation
  }

  @Around("restMetrics() && args(.., @RequestBody body)")
  public Object around(final ProceedingJoinPoint joinPoint, final Object body) throws Throwable {
    final HttpServletRequest request =
        ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();

    data = new ConcurrentHashMap<>();
    data.put("host", request.getRemoteHost());
    data.put("request-method", request.getMethod());
    data.put("request-uri", request.getRequestURI());
    data.put("request-headers", getRequestHeaders(request));

    Optional.of(request)
        .map(HttpServletRequest::getQueryString)
        .ifPresent(queryString ->
            data.put("request-query-params", queryString));

    Optional.of(joinPoint)
        .map(ProceedingJoinPoint::getSignature)
        .map(Signature::getName)
        .ifPresent(name -> data.put("request-method-name", name));

    Optional.ofNullable(body)
        .filter(b -> !request.getMethod().equalsIgnoreCase("GET"))
        .ifPresent(b -> {
          try {
            data.put("request-body", objectMapper.writeValueAsString(b));
          } catch (JsonProcessingException e) {
            data.put("exception-in-request-body", e.getMessage());
          }
        });

    return joinPoint.proceed();
  }

  @AfterReturning(pointcut = "restMetrics()", returning = "result")
  public void afterReturning(final JoinPoint joinPoint, final ResponseEntity<?> result) {
    data = Optional.ofNullable(data)
        .orElse(new ConcurrentHashMap<>());

    Optional.of(result)
        .map(ResponseEntity::getStatusCode)
        .map(HttpStatus::value)
        .ifPresent(statusCode -> data.put("status-code", statusCode));

    Optional.of(result)
        .map(ResponseEntity::getHeaders)
        .ifPresent(headers -> data.put("response-headers", headers));

    Optional.of(result)
        .map(ResponseEntity::getBody)
        .ifPresent(body -> {
          try {
            data.put("response-body", objectMapper.writeValueAsString(body));
          } catch (JsonProcessingException e) {
            data.put("exception-in-response-body", e.getMessage());
          }
        });

    send(MetricType.COMPLETE, getAnnotation(joinPoint)
        .map(RestMetrics::messageComplete)
        .orElse(MetricType.COMPLETE.name()));
  }

  @AfterThrowing(pointcut = "restMetrics()", throwing = "throwable")
  public void afterThrowing(final JoinPoint joinPoint, final Throwable throwable) {
    data = Optional.ofNullable(data)
        .orElse(new ConcurrentHashMap<>());

    Optional.of(joinPoint).map(JoinPoint::getSignature).map(Signature::getName)
        .ifPresent(name -> data.put("method-name", name));

    Optional.of(throwable).map(ExceptionUtils::getThrowableList)
        .filter(l -> !CollectionUtils.isEmpty(l)).ifPresent(messages -> {
      messages.stream().map(ExceptionUtils::getMessage).filter(StringUtils::isNotBlank)
          .map(String::trim).filter(m -> StringUtils.indexOf(m, ":") < m.length() - 1)
          .findFirst().ifPresent(message -> data.put("exception.message", message));

      data.put("exception.cause",
          ExceptionUtils.getMessage(messages.get(messages.size() - 1)).trim());
    });

     send(MetricType.EXCEPTION, getAnnotation(joinPoint)
         .map(RestMetrics::messageException)
         .orElse(MetricType.EXCEPTION.name()));
  }

  private void send(final MetricType type, final String event) {
    if (isLogstash) {
      final var services = new String[]{"LOGSTASH"};
      MetricsService.getServices(services)
          .forEach(service -> service.send(type, event, data));
    }
  }

  private Optional<RestMetrics> getAnnotation(JoinPoint joinPoint) {
    return Optional.of(joinPoint)
        .map(JoinPoint::getSignature)
        .filter(MethodSignature.class::isInstance)
        .map(MethodSignature.class::cast)
        .map(MethodSignature::getMethod)
        .map(method -> method.getAnnotation(RestMetrics.class));
  }

  private String getRequestHeaders(final HttpServletRequest request) {
    final var headers = request.getHeaderNames();
    final var requestHeaders = new StringBuilder();

    Optional.ofNullable(headers).ifPresent(h -> {
      while (h.hasMoreElements()) {
        requestHeaders.append(" ").append(request.getHeader(h.nextElement()));
      }
    });

    return requestHeaders.toString();
  }
}
