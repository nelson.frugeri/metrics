package com.metrics.service;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.marker.Markers;

@Slf4j
@NoArgsConstructor
public enum MetricsService implements Metrics {

  LOGSTASH {
    @Override
    public void send(final MetricType type, final String event,
        final ConcurrentMap<String, Object> data) {
      if (MetricType.EXCEPTION.equals(type)) {
        log.error(Markers.appendEntries(data), event);
      } else {
        log.info(Markers.appendEntries(data), event);
      }
    }
  };

  private static final Map<String, MetricsService> GET_NAMES = getNames();

  public static Stream<MetricsService> getServices(final String[] services) {
    return Arrays.stream(services).map(String::toUpperCase).map(GET_NAMES::get);
  }

  private static Map<String, MetricsService> getNames() {
    return Stream.of(MetricsService.values())
        .collect(Collectors.toMap(MetricsService::name, Function.identity()));
  }
}
