package com.metrics.service;

import java.util.concurrent.ConcurrentMap;

public interface Metrics {
  void send(MetricType type, String event, ConcurrentMap<String, Object> values);
}
