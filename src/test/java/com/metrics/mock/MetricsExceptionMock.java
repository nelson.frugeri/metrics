package com.metrics.mock;

public class MetricsExceptionMock extends RuntimeException {

  private static final long serialVersionUID = 6323368559826547585L;

  MetricsExceptionMock(String message, Exception cause) {
    super(message, cause);
  }
}
