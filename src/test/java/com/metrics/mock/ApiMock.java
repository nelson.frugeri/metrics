package com.metrics.mock;

import com.metrics.component.RestMetrics;
import com.metrics.dto.BodyDto;
import java.util.UUID;
import lombok.SneakyThrows;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/test")
public class ApiMock {

  @RestMetrics
  @GetMapping(value = "/simpleGet")
  public ResponseEntity<BodyDto> simpleGet() {
    return ResponseEntity.status(HttpStatus.OK)
        .body(BodyDto.builder().id(1).build());
  }

  @RestMetrics
  @GetMapping(value = "/withHeader")
  public ResponseEntity<BodyDto> withHeader(@RequestHeader("Authorization") String authorization) {
    final var responseHeaders = new HttpHeaders();

    responseHeaders.set("isAuthorization", "OK");

    return ResponseEntity.status(HttpStatus.OK)
        .headers(responseHeaders)
        .body(BodyDto.builder().id(1).build());
  }

  @RestMetrics
  @GetMapping(value = "/getWithQueryParam")
  public ResponseEntity<BodyDto> getWithQueryParam(@RequestParam final UUID uuid) {
    return ResponseEntity.status(HttpStatus.OK)
        .body(BodyDto.builder().id(1).build());
  }

  @RestMetrics
  @GetMapping(value = "/getWithPathParam/{id}")
  public ResponseEntity<BodyDto> getWithPathParam(@PathVariable("id") final int id) {
    return ResponseEntity.status(HttpStatus.OK)
        .body(BodyDto.builder().id(id).build());
  }

  @RestMetrics
  @PostMapping(value = "/simplePost")
  public ResponseEntity<BodyDto> simplePost(
      @RequestBody final BodyDto bodyDto) {

    return ResponseEntity.status(HttpStatus.CREATED).body(bodyDto);
  }

  @RestMetrics
  @PostMapping(value = "/postWithQueryParam")
  public ResponseEntity<BodyDto> postWithQueryParam(
      @RequestParam final int id,
      @RequestBody final BodyDto bodyDto) {

    return ResponseEntity.status(HttpStatus.CREATED).body(bodyDto);
  }

  @RestMetrics()
  @PostMapping(value = "/postWithPathParam/{id}")
  public ResponseEntity<BodyDto> postWithPathParam(
      @PathVariable final int id,
      @RequestBody final BodyDto bodyDto) {

    return ResponseEntity.status(HttpStatus.CREATED).body(bodyDto);
  }

  @RestMetrics
  @SneakyThrows
  @GetMapping(value = "/exception")
  public ResponseEntity<BodyDto> whenException() {
    throw new MetricsExceptionMock("Message", new Exception("Cause"));
  }

  @RestMetrics(messageComplete = "LOG_INFO")
  @GetMapping(value = "/simpleGetWithAnnotationParam")
  public ResponseEntity<BodyDto> simpleGetWithAnnotationParam() {
    return ResponseEntity.status(HttpStatus.OK)
        .body(BodyDto.builder().id(1).build());
  }

  @RestMetrics(messageException = "LOG_EXCEPTION")
  @SneakyThrows
  @GetMapping(value = "/exceptionWithAnnotationParam")
  public ResponseEntity<BodyDto> whenExceptionWithAnnotationParam() {
    throw new MetricsExceptionMock("Message", new Exception("Cause"));
  }
}
