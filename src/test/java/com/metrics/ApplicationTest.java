package com.metrics;

import org.junit.jupiter.api.Test;

public class ApplicationTest {
  @Test
  public void main() {
    Application.main(new String[] {"--spring.config.location=classpath:application-test.yml"});
  }
}
