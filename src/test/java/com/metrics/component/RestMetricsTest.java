package com.metrics.component;

import static java.text.MessageFormat.format;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.metrics.dto.BodyDto;
import com.metrics.mock.ApiMock;
import java.util.UUID;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.aop.aspectj.annotation.AnnotationAwareAspectJAutoProxyCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

@Import(AnnotationAwareAspectJAutoProxyCreator.class)
@WebMvcTest(controllers = ApiMock.class)
@TestPropertySource(properties = {"spring.config.location=classpath:application-test.yml"})
class RestMetricsTest {

  @Autowired
  private MockMvc mockMvc;

  @SpyBean
  private Metrics metrics;

  private static final String ENDPOINT = "v1/test";

  @Test
  void simpleGet() throws Throwable {
    mockMvc.perform(get(
        format("/{0}/{1}", ENDPOINT, "/simpleGet")))
        .andReturn();

    verify(metrics, times(0)).around(any(), any());
    verify(metrics, times(1)).afterReturning(any(), any());
  }

  @Test
  void withRHeader() throws Throwable {
    mockMvc.perform(get(
        format("/{0}/{1}", ENDPOINT, "/withHeader"))
        .header("Authorization", UUID.randomUUID()))
        .andReturn();

    verify(metrics, times(1)).around(any(), any());
    verify(metrics, times(1)).afterReturning(any(), any());
  }

  @Test
  void getWithQueryParam() throws Throwable {
    mockMvc.perform(get(
        format("/{0}/{1}", ENDPOINT, "/getWithQueryParam"))
        .queryParam("uuid", "d22f881c-6eb1-4c2f-87d5-6fe558136194"))
        .andReturn();

    verify(metrics, times(1)).around(any(), any());
    verify(metrics, times(1)).afterReturning(any(), any());
  }

  @Test
  void getWithPathParam() throws Throwable {
    mockMvc.perform(get(
        format("/{0}/{1}/{2}", ENDPOINT, "/getWithPathParam", 1)))
        .andReturn();

    verify(metrics, times(1)).around(any(), any());
    verify(metrics, times(1)).afterReturning(any(), any());
  }

  @Test
  void simplePost() throws Throwable {
    mockMvc.perform(post(
        format("/{0}/{1}", ENDPOINT, "/simplePost"))
        .contentType(MediaType.APPLICATION_JSON)
        .content(new ObjectMapper().writeValueAsString(BodyDto.builder().id(1).build())))
        .andReturn();

    verify(metrics, times(1)).around(any(), any());
    verify(metrics, times(1)).afterReturning(any(), any());
  }

  @Test
  void postWithQueryParam() throws Throwable {
    mockMvc.perform(post(
        format("/{0}/{1}", ENDPOINT, "/postWithQueryParam"))
        .queryParam("id", "1")
        .contentType(MediaType.APPLICATION_JSON)
        .content(new ObjectMapper().writeValueAsString(BodyDto.builder().id(1).build())))
        .andReturn();

    verify(metrics, times(1)).around(any(), any());
    verify(metrics, times(1)).afterReturning(any(), any());
  }

  @Test
  void postWithPathParam() throws Throwable {
    mockMvc.perform(post(
        format("/{0}/{1}/{2}", ENDPOINT, "/postWithPathParam", 1))
        .contentType(MediaType.APPLICATION_JSON)
        .content(new ObjectMapper().writeValueAsString(BodyDto.builder().id(1).build())))
        .andReturn();

    verify(metrics, times(1)).around(any(), any());
    verify(metrics, times(1)).afterReturning(any(), any());
  }

  @Test
  @SneakyThrows
  void whenException() throws Throwable {
    mockMvc.perform(get(
        format("/{0}/{1}", ENDPOINT, "/exception"))
        .contentType(MediaType.APPLICATION_JSON))
        .andReturn();

    verify(metrics, times(0)).around(any(), any());
    verify(metrics, times(0)).afterReturning(any(), any());
    verify(metrics, times(1)).afterThrowing(any(), any());
  }

  @Test
  void simpleGetWithAnnotationParam() throws Throwable {
    mockMvc.perform(get(
        format("/{0}/{1}", ENDPOINT, "/simpleGetWithAnnotationParam")))
        .andReturn();

    verify(metrics, times(0)).around(any(), any());
    verify(metrics, times(1)).afterReturning(any(), any());
  }

  @Test
  @SneakyThrows
  void whenExceptionWithAnnotationParam() throws Throwable {
    mockMvc.perform(get(
        format("/{0}/{1}", ENDPOINT, "/exceptionWithAnnotationParam"))
        .contentType(MediaType.APPLICATION_JSON))
        .andReturn();

    verify(metrics, times(0)).around(any(), any());
    verify(metrics, times(0)).afterReturning(any(), any());
    verify(metrics, times(1)).afterThrowing(any(), any());
  }
}
